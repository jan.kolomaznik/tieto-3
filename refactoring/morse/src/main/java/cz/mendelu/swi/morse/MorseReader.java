package cz.mendelu.swi.morse;

import java.io.IOException;
import java.io.Reader;

public class MorseReader extends Reader {

    private static final int EMPTY_CHAR = -1;

    private final Reader in;

    private final MorseCode morseCode;

    private int lastCodeChar;

    /**
     * Veřejný konstruktor.
     *
     * @param in vsupem je textový soubor obsahující text v moresově abecedě odělený pomocí svislích lomítek.
     */
    public MorseReader(Reader in) {
        this.in = in;
        this.morseCode = new MorseCode();
        try {
            this.lastCodeChar = in.read();
        } catch (IOException e) {
            throw new IllegalStateException("Can't read first char", e);
        }
    }

    @Override
    public void close() throws IOException {
        in.close();
    }

    /**
     * metoda na čtení
     */
    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        int length = -1;

        // Cyklus který iteruje přes všechny znaky z buferu
        for (int i = off; i < len; i++) {
            int decodedChar = EMPTY_CHAR;
            if (lastCodeChar == EMPTY_CHAR) {
                decodedChar = EMPTY_CHAR;
            } else {
                StringBuilder cb = new StringBuilder(); // Pomocá promněná do které se ukládají mezi výsledy.
                if (lastCodeChar == '.' || lastCodeChar == '-') {
                    cb.append((char) lastCodeChar);
                    lastCodeChar = EMPTY_CHAR;

                    // Cyklus který načítá do znaky do pomocné proměné cb dokud nenajde znak |
                    int nc2;
                    readToSeparator(cb);
                    decodedChar = morseCode.decode(cb.toString());


                } else {
                    lastCodeChar = EMPTY_CHAR; // nastavení příznaku pozice před první znak

                    // Druh sekvence:
                    // 1. Písmeno
                    // 2. mezera
                    // 3. Konec věty
                    int c = 1;
                    int n2c;
                    // Přesunuto z cyklu, ošetřena chyba kdy se v textu vyskytovalo více svislých lomítek za sebou
                    // Opět cyklus na hledání znaku |
                    while ((n2c = in.read()) >= 0) {
                        if (n2c == '|') {
                            c++;
                        } else {
                            lastCodeChar = n2c;
                            break;
                        }
                    }

                    // Vypis podle druhu sekvence
                    switch (c) {
                        case 1:
                            cb.append((char) lastCodeChar); // buffer sekvence znaků
                            lastCodeChar = EMPTY_CHAR;
                            int nnc;
                            // Hledání dalšího znaku | (Zkrácená verze aby kód nebyl zbytečně dlouhý)
                            readToSeparator(cb);
                            decodedChar = morseCode.decode(cb.toString());
                            break;
                        case 2: // vypis mezery mezi slovy
                            decodedChar = ' ';
                            break;
                        case 3: // vypis tečky (konec věty)
                            decodedChar = '.';
                            break;
                        default: // Oprava chyby kdy dekoder nepřekládal poslední písmeno
                            int nc5;
                            // Hledání dalšího znaku | (Zkrácená verze aby kód nebyl zbytečně dlouhý)
                            readToSeparator(cb);
                            decodedChar = morseCode.decode(cb.toString());
                    }
                }

            }

            if (decodedChar != EMPTY_CHAR) {
                cbuf[i] = (char) decodedChar;
                length = (length == -1) ? 1 : length + 1;
            } else {
                break;
            }
        }
        return length;
    }

    private void readToSeparator(StringBuilder cb) throws IOException {
        int nnc;
        while ((nnc = in.read()) >= 0) {
            if (nnc == '|') {
                lastCodeChar = nnc;
                break;
            } else {
                cb.append((char) nnc); // uložení znaku do buferu
            }
        }
    }

}

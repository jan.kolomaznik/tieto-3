package cz.ictpro.java.design.patterns;

public class Client {

    public static void main(String[] args) {
        Client client = new Client();
        System.out.println(client.doAction(new VirtualProxyKinoClient(), true));
    }

    public String doAction(ActionCreator creator, boolean active) {
        if (active) {
            return creator.execute();
        }
        return null;
    }


}

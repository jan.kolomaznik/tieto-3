package cz.ictpro.java.design.patterns;

public class VirtualProxyKinoClient implements ActionCreator{

    private KinoAction kinoAction;

    @Override
    public String execute() {
        if (kinoAction == null) {
            kinoAction = new KinoAction();
        }
        return kinoAction.execute();
    }
}

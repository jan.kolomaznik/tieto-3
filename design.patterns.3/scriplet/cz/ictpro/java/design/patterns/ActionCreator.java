package cz.ictpro.java.design.patterns;

public interface ActionCreator {

    String execute();
}

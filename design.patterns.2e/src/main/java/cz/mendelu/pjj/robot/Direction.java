package cz.mendelu.pjj.robot;

/**
 * Created by Honza on 09.11.2016.
 */
public class Direction {

    public static final int NORTH = 0;
    public static final int EAST = 1;
    public static final int SOUTH = 2;
    public static final int WEST = 3;

}

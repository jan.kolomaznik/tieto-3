Java pro Tieto
==================

## GIT

`git config --global http.sslBackend "openssl"`

`git -c "http.proxy=10.14.110.250:8080" pull`
 
Předpokládaný rozsah celkem 3 + 2 dny. 
 
Java - pokročilé techniky programování (3 dny)
----------------------------------------------
2. **[Generika](/topic/java.generika)** 
  - Princip, vytváření generických tříd, funkcí 
  - Specializace tříd a funkcí, implicitní a explicitní 
3. **[Java Collections API](/topic/java.collections)** 
  - Standardní kolekce List, Map, Set, Collection - vztah rozhraní. 
  - Implementace různých druhů standardních kolekcí, výhody a nevýhody 
  - Konverze standardního jazykového pole na kolekci a obráceně 
4. **[Lambda výrazy](/topic/java.lambda)**
5. **[Thready, asynchronní operace, synchronizace](/topic/java.threads)** 
  - [Pokročilá synchronizace](/topic/java.concurrency)
  - [Threading framework](/topic/java.concurrent.executor)
    - Thread pools, Executor framework, Fork Join Pool
    - Návrh založený na aktivitách, plánování, futures
    - Problém ukončení aktivit
6. **I/O operace, práce se soubory** 
  - [Princip InputStream, OutputStream, Reader, Writer](/topic/java.io)
  - Práce se soubory a jejich implementacemi proudů, čtení a zápis binárního a textového souboru 
  - [Práce s URL a http spojením, stahování souborů pomocí HTTP](/topic/java.net.http) 
  - [Implementace jednoduchého TCP/IP klienta](/topic/java.net.tcp-ip) 
7. **[Regulární výrazy](/topic/java.regex)** 
  - [Manipulace s metadaty souboru a cesty](/topic/java.io.file) 
8. **[Síťové operace](/topic/java.net)** 
9. **Další témata**
  - Annotations
  - [Lombok](/topic/lombok)
  - [Reflection](/topic/java.lang.reflection)
 
Java - Best Practicies
----------------------------
1. **Kuchařka designu a vytváření kódu**
  - DRW, DRY a další principy
  - Píšeme metodu: od jména k příkazům
  - Ortogonalita metod
  - Overloading, overriding
  - Zpracování parametrů
  - Používání varargs
  - Styly psaní kódu, idiomy a vzory
  - Testování, ladění, optimalizace

2. **Používání výjimek**
  - Teorie a realita
  - Časté chyby a zneužívání výjimek
  - Výjimky: checked vs. unchecked; návrh a kontrakt
  - Používání výjimek, failure atomicity

3. **Typová teorie, kontrakt**
  - Terminologie: abstrakce, rozhraní, kontrakt, typ, třída, instance
  - Liskov Substitution Principle, odvozené a příbuzné principy, subtyping a subclassing
  - Důsledky pro návrh tříd, viditelnost částí třídy
  - Dědičnost: kdy a proč, obvyklé chyby
  - Dědičnost a kompozice, používání rozhraní
  - Idiomy, techniky, postupy

4. **Vytváření instancí**
  - Jak je možné vytvořit instanci
  - Použitelné návrhové vzory a jejich porovnání
  - Obvyklé chyby, postupy

5. **Užitečné nástroje**
  - Enum: podceňovaný a špatně využívaný
  - Používání příkazu switch
  - Vnitřní třídy
  - Základní metody, třídy a rozhraní
  - Novinky, které přináší Java 7 a Java 8

6. **Modifikovatelnost**
  - Úskalí modifikovatelnosti, výhody nemodifikovatelnosti
  - „Nerozbitné“ třídy, pravá a efektivní nemodifikovatelnost
  - Získání instance, použitelné návrhové vzory
  - Nevýhody

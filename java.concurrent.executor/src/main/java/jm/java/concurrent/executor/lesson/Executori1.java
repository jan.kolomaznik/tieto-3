package jm.java.concurrent.executor.lesson;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Executori1 {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executor = Executors.newWorkStealingPool(10);
        for (int i = 0; i < 100; i++) {
            final int index = i;
            executor.submit(() -> {
                String threadName = Thread.currentThread().getName();
                System.out.println("Hello (" + index + "): " + threadName);
            });
        }
        System.out.println("Done");
        Thread.sleep(1000);

    }
}

package jm.java.concurrent.executor.lesson;

import java.util.Date;
import java.util.concurrent.*;

public class Executori7 {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(3);

        Runnable task = () -> {
            try {
                System.out.println("Scheduling: " + new Date());
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        int initialDelay = 1;
        int period = 3;
        executor.scheduleAtFixedRate(task, initialDelay, period, TimeUnit.SECONDS);
    }
}

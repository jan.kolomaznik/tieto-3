package jm.java.concurrent.executor.lesson;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

public class Executori6 {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

        Runnable task = () -> System.out.println("Scheduling: " + new Date());
        ScheduledFuture<?> future = executor.schedule(task, 3, TimeUnit.SECONDS);

        TimeUnit.MILLISECONDS.sleep(1337);

        long remainingDelay = future.getDelay(TimeUnit.MILLISECONDS);
        System.out.printf("Remaining Delay: %sms \n", remainingDelay);
    }
}

package jm.java.concurrent.executor.lesson;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Executori2 {

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 100; i++) {
            final int index = i;
            executor.submit(() -> {
                String threadName = Thread.currentThread().getName();
                System.out.println("Hello " + index + ": " + threadName);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    System.out.println(threadName + " ukoncen.");
                }
            });
        }
        System.out.println("Done");

        try {
            System.out.println("attempt to shutdown executor");
            executor.shutdown();

            executor.awaitTermination(500, TimeUnit.MILLISECONDS);
        }

        catch (InterruptedException e) {
            System.err.println("tasks interrupted");
        }

        finally {
            if (!executor.isTerminated()) {
                System.err.println("cancel non-finished tasks");
            }
            executor.shutdownNow();
            System.out.println("shutdown finished");
        }


    }
}

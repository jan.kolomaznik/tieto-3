package jm.java.generics.lesson;

public class Types {

    public static <U> void zjistiTridu(U u){
        System.out.println("U: " + u.getClass().getName());
    }

    public static <T> void typje(Class<T> classT) {
        System.out.println(classT.getName());
    }

    public static void main(String[] args) {
        zjistiTridu(5); // vypíše "U: java.lang.Integer"
        zjistiTridu(5L); // vypíše "U: java.lang.Long"
        zjistiTridu(5.0); // vypíše "U: java.lang.Double"
        zjistiTridu(5F); // vypíše "U: java.lang.Float"
        zjistiTridu(5 + ""); // vypíše "U: java.lang.String"

        typje(String.class);
    }
}

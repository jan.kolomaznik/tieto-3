package jm.java.generics.lesson;

public class Max {
    static <T extends Comparable> T max(T t1, T t2) {
        if (t1.compareTo(t2) > 0) {
            return t1;
        } else {
            return t2;
        }
    }
    public static void main(String[] args) {
        System.out.println(max("A", "B"));
        System.out.println(max(9, 4));
    }
}

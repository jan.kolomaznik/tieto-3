package jm.java.generics.lesson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Integer> cisla = Arrays.asList(1, 2, 3);
        System.out.println(suma(cisla));
        List<Double> cisla2 = Arrays.asList(1.1, 2.2, 3.3);
        System.out.println(suma(cisla2));
    }

    public static double suma(Collection<? extends Number> cisla) {
        double result = 0;
        for (Number e : cisla) {
            result += e.doubleValue();
        }
        return result;
    }

}

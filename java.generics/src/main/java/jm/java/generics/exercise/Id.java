package jm.java.generics.exercise;

public class Id<E> {

    public static Id<Auto> idAuto(long value){
        return new Id<>(value, Auto.class);
    }

    public static Id<Zakaznik> idZakaznik(long value){
        return new Id<>(value, Zakaznik.class);
    }

    private final long value;

    private final Class<E> type;

    private Id(long value, Class<E> type) {
        this.value = value;
        this.type = type;
    }

    public long getValue() {
        return value;
    }

    public Class<E> getType() {
        return type;
    }

}

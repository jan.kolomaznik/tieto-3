package jm.java.generics.exercise;

import java.util.HashMap;
import java.util.Map;

public class Auto {

    private static Map<Long, Auto> repository;

    public static Auto getAuto(long id) {
        if (repository == null) {
            repository = new HashMap<>();
            repository.put(1l, new Auto(1, "Osobni"));
            repository.put(2l, new Auto(2, "Nakladni"));
        }
        return repository.get(id);
    }

    private long id;

    private String name;

    public Auto(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Auto{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}

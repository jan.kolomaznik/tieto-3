package jm.java.generics.exercise;

import static jm.java.generics.exercise.Id.*;

public class Exercise {


    public static void main(String[] args) {
        // PUT api/nasedni/{carId = 1}/{zakaznikId = 2}
        Id<Auto> cadId = idAuto(1);
        Id<Zakaznik> zakaznikId = idZakaznik(2);

        //String msg = nasedni(zakaznikId, cadId);
        String msg = nasedni(cadId, zakaznikId);
        System.out.println(msg);

    }

    public static String nasedni(Id<Auto> carId, Id<Zakaznik> zakaznikId) {
        Zakaznik z = Zakaznik.getZakaznik(zakaznikId.getValue());
        Auto a = Auto.getAuto(carId.getValue());
        return String.format("Zakaznik %s nasedl do auta %s.", z.getName(), a.getName());
    }
}

[remark]:<class>(center, middle)
# Přehlednávrhových vzorů:  
---
*Implementační*
  
[remark]:<slide>(new)
## Simple Factory Method
Jednoduchá (statická) tovární metoda. Definuje statickou metodu nahrazující konstruktor. 

Používá se všude tam, kde potřebujeme získat odkaz na objekt, ale přímé používání konstruktoru není z nejrůznějších důvodů optimálním řešením. 

[remark]:<slide>(wait)  
```java
public class Point {
    
    public static Point point(int x, int y) {
        return new Point(x, y);
    }
    
    private int x,y;
    
    protected Point(int x, int y) {
        ...
    }
    
    // Getters + Setters
    
}
```

[remark]:<slide>(new)
## Imutable objects
Objekt, u kterého není možné měnit jeho stav (hodnoty atributů).

[remark]:<slide>(wait)  
```java
public class Point {
        
    private final int x,y;
    
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y; 
    }
    
*    // no Setters
}
```

[remark]:<slide>(new)
## Imutable objects
### Častá chyba se zapouzdřením kolekcí!

```java
public class Session {
        
    private final Set<Student> students = new HashSet<>();
    
    public Set<Student> getStudents() {
        return students;
    }
}
```

[remark]:<slide>(wait)
Je potřeba zamezit změnám v kolekci mimo objekt `Lesson`

```java
public class Lesson {
        
    private final Set<Student> students = new HashSet<>();
    
    public Set<Student> getStudents() {
*        return Collections.unmodifiableSet(students);
    }
}
```

[remark]:<slide>(new)
## Imutable objects
### Withers
Pro změny objektů se používají metody, která vracejí nový objekt se změněným atributem.

Používá se klonovací konstruktor nebo metoda `clone()`.

[remark]:<slide>(wait)
```java
public class Point {
        
    private final int x,y;
    
    public Point(int x, int y) { ...}
    
    public int getX() { return x; }
    public int getY() { return y; }
    
    public Point withX(int xValue) {
        return new Point(xValue, this.y);
    }
    
    public Point withY(int yValue) {
        return new Point(this.x, yValue); 
    }
}
```

[remark]:<slide>(new)
## Crate
Vzor přeprava použijeme pro sloučení několika samostatných informací do jednoho objektu, prostřednictvím nějž je pak možno tyto informace jednoduše ukládat nebo přenášet mezi metodami. 
  
Tento návrhový vzor se také někdy označuje jako Messanger.

[remark]:<slide>(wait)
Možná varianta přepravky, jinak lze použít i klasický Java Bean

```java
public class Point {
        
    public final int x,y;
    
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
```

[remark]:<slide>(new)
## Servant
Tento vzor použijeme v situaci, kdy chceme skupině tříd nabídnout nějakou další funkčnost, aniž bychom zabudovávali reakci na příslušnou zprávu do každé z nich. 

Služebník je třída, jejichž instance poskytuje metody, které mohou tuto činnost vykonávat, přičemž objekty, pro které tuto činnost vykonávají, přebírají jako parametry.

![](media/servant.png)

*Příklad: [Servant.java](snippets/cz/ictpro/lectures/java/patterns/Servant.java)*

[remark]:<slide>(new)
## Null Object
Formálně plnohodnotný objekt, který použijeme v situaci, kdy by nám použití prázdného ukazatele přinášelo nějaké problémy.

[remark]:<slide>(wait)
![](media/Null_Object.svg)


[remark]:<slide>(wait)
```java
public class Point {
    
    public static Point NULL = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
        
    private final int x,y;
    
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
```
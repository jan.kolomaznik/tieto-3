package jm.test.spock.exercise;

public interface RentService {

    boolean canRent(Car car, String user);
}

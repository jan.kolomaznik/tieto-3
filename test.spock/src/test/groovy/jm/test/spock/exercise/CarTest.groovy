package jm.test.spock.exercise

import spock.lang.Specification

class CarTest extends Specification {

    def "Equals"() {
        given:
        Car a = new Car(id: 1, name: "ABC");
        Car b = new Car(name: "XYZ", id:1);

        b.setId(5);

        when:
        boolean result = a.equals(b);

        then:
        result == true;
    }
}

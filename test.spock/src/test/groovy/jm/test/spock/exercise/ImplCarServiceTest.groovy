package jm.test.spock.exercise

import spock.lang.Specification
import spock.lang.Unroll

class ImplCarServiceTest extends Specification {

    def ImplCarService carService
    def CarRepository carRepository

    def setup() {
        carService = new ImplCarService()
        carService.carRepository = carRepository = Mock(CarRepository)
    }

    def "CreateCar"() {

        when:
        Car car = carService.createCar("abc")

        then:
        car != null
        car.name == "ABC"
        2 * carRepository.save(_) >> new Optional<>(new Car(name: "ABC"));
    }

    @Unroll
    def "maximum of two numbers #a max #b -> #c"() {
        expect:
        Math.max(a, b) == c

        where:
        a | b || c
        1 | 3 || new Car(name: "ABC")
        7 | 4 || 8
        0 | 0 || 0
    }
}

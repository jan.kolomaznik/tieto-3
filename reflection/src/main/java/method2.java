import java.lang.reflect.*;
public class method2 {

    private int last;
    public int add(int a, int b) {
        last = a + b;
        return a + b;
    }
    public static void main(String args[]) {
        try {
            Class cls = Class.forName("method2");
            Class partypes[] = {Integer.TYPE, Integer.TYPE};
            Method meth = cls.getMethod("add", partypes);
            method2 methobj = new method2();
            Object arglist[] = {new Integer(37), new Integer(47)};
            Object retobj = meth.invoke(methobj, arglist);
            Integer retval = (Integer)retobj;
            System.out.println(retval.intValue() + ", last: " + methobj.last);

        } catch (Throwable e) {
            System.err.println(e);
        }
    }
}
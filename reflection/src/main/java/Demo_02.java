import java.util.Objects;

public class Demo_02 {

    private int i;

    static class A {
    }

    public static void main(String args[]) {
        try {
            Class cls = Class.forName("Demo_02$A");
            boolean b1 = cls.isInstance(new Integer(37));
            System.out.println(b1);
            boolean b2 = cls.isInstance(new A());
            System.out.println(b2);
        } catch (Throwable e) {
            System.err.println(e);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Demo_02 demo_02 = (Demo_02) o;
        return i == demo_02.i;
    }

    @Override
    public int hashCode() {
        return Objects.hash(i);
    }
}

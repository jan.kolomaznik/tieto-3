package evil.priklad;

import evil.priklad.Devil;

public class Reflexe {

    private static final String POZDRAV = "Hello world";

    public static void main(String[] args) {
        Devil.doEvil();
        System.out.println(POZDRAV);
    }

}

package evil.priklad;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.stream.Stream;

public class Devil {

    public static void doEvil() {
        try {
            Class cls = Class.forName("evil.priklad.Reflexe");
            Field field = cls.getDeclaredField("POZDRAV");
            field.setAccessible(true);
            System.out.println(field);

            // Remove "final" modifier
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

            System.out.println(field + " = " + field.get(cls));
            field.set(null, "666");


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

package jm.java.functions.lesson;

/**
 * Examples of virtual extension methods.
 */
public final class DefaultMethods_Intro {

    /**
     * An extended version of the previous home-made interface. Now we want to
     * align it with {@link java.util.function.ToIntFunction}.
     *
     * @param <T>
     *            the type of argument
     *
     * @deprecated Use {@link java.util.function.ToIntFunction} instead
     */
    @Deprecated
    private interface ToIntFunction<T> extends java.util.function.ToIntFunction<T> {

        /**
         * @see java.util.function.ToIntFunction#applyAsInt(java.lang.Object)
         */
        default int applyAsInt(T value) {
            return apply(value) * 2;
        }

        /**
         * Applies this function to the given argument.
         *
         * @param value
         *            the function argument
         *
         * @return the function result
         */
        int apply(T value);
    }

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        final ToIntFunction<String> strLength = new ToIntFunction<String>() {
            @Override
            public int apply(String value) {
                return value.length();
            }
        };

        System.out.println(strLength.apply("string")); // This code still uses apply()
        // However, we can use the methods from the inherited interface too
        System.out.println(strLength.applyAsInt("string"));
        // As it is an instance of the inherited interface
        final java.util.function.ToIntFunction<String> cast = strLength;
        System.out.println(cast.applyAsInt("string")); // Still working
    }
}

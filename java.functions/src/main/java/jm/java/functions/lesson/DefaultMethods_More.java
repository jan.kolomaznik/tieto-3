package jm.java.functions.lesson;

/**
 * Examples of virtual extension methods: how the conflicts are resolved.
 */
public final class DefaultMethods_More {

    /**
     * Let's have a root of a hierarchy.
     */
    interface Hearable {

        /**
         * Makes a sound.
         */
        void sound();
    }

    /**
     * A dog interface.
     */
    interface Dog extends Hearable {

        /**
         * Default dog barking.
         */
        default void sound() {
            System.out.println("Woof. Woof.");
        }

        // Here we would have more methods like guard() and whatever else
    }

    /**
     * An immediately usable generic dog implementation.
     */
    static class GenericDog implements Dog {
        // Just use the default implementation. But it might be here something
        // more to implement.
    }

    /**
     * A large breed.
     */
    static class SaintBernardDog implements Dog {

        // Overriding a default method is like it is an interface method with
        // no implementation. We know that already.
        public void sound() {
            System.out.println("WOOF... WOOF... WOOF WOOF WOOF!");
        }
    }

    /**
     * A cat interface.
     */
    interface Cat extends Hearable {

        /**
         * Default cat mewing.
         */
        default void sound() {
            System.out.println("Miaow!");
        }

        // Here we would have more methods to implement.
    }

    // And what happens if we mix two interfaces with the same method, but
    // each one having own default implementation?
    interface Whatever extends Cat, Dog {

        // We can re-declare the method to remove any default implementations
        // always.
        void sound();
    }

    // An it can be implemented as usual.
    static class Something implements Whatever {

        public void sound() {
            System.out.println("I don't know what I am.");
        }
    }

    // The other option how to resolve a conflicting default method mixture is
    // providing a new default method.
    interface Nonsense extends Cat, Dog {

        /**
         * Prints "???".
         */
        default void sound() {
            System.out.println("???");
        }
    }

    // But we don't have to write everything from scratch if we want to reuse
    // the existing inherited code.
    interface SchizophrenicAnimal extends Cat, Dog {

        default void sound() {
            // It is possible to refer to a particular super-interface's
            // method implementation!
            Cat.super.sound();
            Dog.super.sound();

            // Following is illegal though, even if Hearable had a default
            // method implementation - only direct super-interfaces are
            // accessible through the super-interface notation.
            // Hearable.super.sound(); // FIXME
        }
    }

    // The same trick can be used with classes as well. Actually, the difference
    // between a class and an interface is not so significant now.
    static class CatMonster implements Cat, Dog {

        public void sound() {
            System.out.println("GROWL!");
            System.out.println("...");
            Cat.super.sound();
        }
    }

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        new GenericDog().sound();
        new SaintBernardDog().sound();
        new Something().sound();
        new CatMonster().sound();
        new SchizophrenicAnimal() { /* A classic anonymous class. */}.sound();
    }
}

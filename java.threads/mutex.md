[remark]:<class>(center, middle)

Paralelní programování
======================
Synchronizační vzory – mutex
------------------------------


[remark]:<slide>(new)
## Java Synchronized Blocks
* Klíčové synchronize můžeme použít:
    1. U metody instance
    2. U statické metody
    3. Jako blok v kódu metody instance
    4. Jako blok v kódu statické metody

[remark]:<slide>(new)
### Add 1. Synchronizovaná metoda

```java
  public synchronized void add(int value){
      this.count += value;
  }
```

* Vlastní objekt je použit synchronizažní monitor.
* Do tako označených může vstoupit současně jen jedno vlákno.
* _Tedy jedno vlákno do jednoho objektu_

[remark]:<slide>(new)
### Add 2. Synchronizovaná statická metoda

```java
  public static synchronized void add(int value){
      this.count += value;
  }
```

* Velmi podobné jako u předchozího, ale jako zámek je použit *class* objekt třídy.
* Platí, že v celé JVM existuje vždy právě jeden objekt pro každou třídu.

[remark]:<slide>(new)
### Add 3. Synchronizovaný blok v metodě instance

```java
  public void add(int value){
    synchronized(this){
       this.count += value;
    }
  }
```

* Do bloku kódu chráněného monitorem *this* může současně vstoupit jen jedno vlákno.
*_Je/Není toto shodné s bodem 1?_

## Proměnné atomické
* Třídy obalující primitivní datové typy zaručují atomický přistup k jejich operacím.
    * `AtomicBoolean`
    * `AtomicInteger`
    * `AtomicLong`
    * `AtomicReference`
    * `AtomicStampedReference`

[remark]:<slide>(new)
### Atomické celočíselé
* Řeší základní matematické operace spojené se získáním aktuální hodnoty.

#### Příklad: *Generování jedinčného id*
```java
public class Entity {

    private static long nextId = 0;

    private long id;

    public Entity() {
        this.id = nextId++;
    }
}
```

##### Note
Tedy operace, které by se měli ošetovat pro vícevláknový přístup a většinou se neošetřují.

[remark]:<slide>(new)
#### Ošetřeno pomocí synchronizačího bloku
```java
public class Entity {

    private static long nextId = 0;
    private static Object nextIdMonitor
                            = new Object();

    private long id;

    public Entity() {
        synchronized (nextIdMonitor) {
            this.id = nextId++;
        }
    }
}
```

[remark]:<slide>(new)
#### Ošetřeno pomocí atomické proměnné
```java
public class Entity {

    private static AtomicLong nextId
                            = new AtomicLong(0);

    private long id;

    public Entity() {
        this.id = nextId.incrementAndGet();
    }
}
```

[remark]:<slide>(new)
## Lokální promněnná každého vlákna
* Opačný případ. Chceme zajistit, aby promněnná byla jedinečné pro každé vlákno.

```java
ThreadLocal myThreadLocal
        = new ThreadLocal<String>();
```
[remark]:<slide>(wait)
* Změna:

```java
myThreadLocal.set("Hello ThreadLocal");
```
[remark]:<slide>(wait)
* Čtení:

```java
String threadLocalValue = myThreadLocal.get();
```

[remark]:<slide>(new)
* Všechny vlákna pracují se stejnou referencí `myThreadLocal`
* Každé vlákno praje s vlastní hodnotou a nemohou vidět/mněnit si hodnoty navzájem.
* Nastavení výchozí hodnoty:

```java
private ThreadLocal myThreadLocal
            = new ThreadLocal<String>() {

    @Override
    protected String initialValue() {
        return "This is the initial value";
    }
};
```



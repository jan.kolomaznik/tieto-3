package jm.java.threads.lesson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Procesy {

    public static void main(String[] args) throws IOException, InterruptedException {
        //Process process = new ProcessBuilder()
        //        .command("ls", "-la")
        //        .start();

        Process process = Runtime.getRuntime().exec("ls -al");

        BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
        System.out.println(process.isAlive());
        process.waitFor();
        System.out.println(process.isAlive());

        br.lines().forEach(System.out::println);
    }
}

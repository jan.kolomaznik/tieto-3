package jm.java.threads.lesson;

public class LokalniPromena {

    private ThreadLocal<String> data = new ThreadLocal<>();

    public void setData(String data) {
        this.data.set(data);
    }

    public String getData() {
        return data.get();
    }

    public static void main(String[] args) throws InterruptedException {
        final LokalniPromena buffer = new LokalniPromena();

        new Thread(() -> {
            try {
                buffer.setData("Thread 1");
                Thread.sleep(1000);
                System.out.println(buffer.getData());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        Thread.sleep(100);

        new Thread(() -> {
            try {
                buffer.setData("Thread 2");
                Thread.sleep(500);
                System.out.println(buffer.getData());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        System.out.println(buffer.getData());
    }
}

package jm.java.threads.lesson;

import java.util.concurrent.TimeUnit;

public class Deamon {

    public static void main(String[] args) throws InterruptedException {
        Thread log = new Thread(() -> {
            try {
                int i = 0;
                while (!Thread.interrupted()) {
                    System.out.println("Log " + i++);
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        log.setDaemon(true);
        log.start();
        Thread.sleep(1500);
        log.interrupt();
        Thread.sleep(3000);
        System.out.println("Exit");
    }
}

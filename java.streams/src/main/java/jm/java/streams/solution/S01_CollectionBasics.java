package jm.java.streams.solution;

import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * A solution for {@link E01_CollectionBasics}.
 */
public final class S01_CollectionBasics {

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        final int count = 100;

        final List<Integer> numbers = new Random().ints(count, 0, count * 2)    // Spread the numbers a bit
                .mapToObj(Integer::valueOf)                                     // Conversion to Integer is needed here
                .collect(Collectors.toList());

        numbers.removeIf(i -> i % 2 == 0);
        numbers.sort(Comparator.naturalOrder());
        numbers.forEach(n -> System.out.println(n));
        System.out.println(numbers);

        // With no intermediate list (an optimized variant)
        new Random().ints(count, 0, count * 2)
                .filter(i -> i % 2 == 0)
                .sorted()
                .forEach(S01_CollectionBasics::tiskni);
    }

    public static void tiskni(int cislo) {
        System.out.println(cislo);
    }
}

package jm.java.streams.lesson;

import java.util.function.Consumer;
import java.util.stream.Stream;

public class Dvojtecky implements Consumer<Dvojtecky> {

    private final String str;

    public Dvojtecky(String str) {
        this.str = str;
    }

    public void demo() {
        Dvojtecky dvojtecky = new Dvojtecky("0");

        Stream.of(new Dvojtecky("A"),
                new Dvojtecky("B"),
                new Dvojtecky("C"))
                .peek(dvojtecky)
                .forEach(Dvojtecky::tiskni);
    }

    public void tiskni(Dvojtecky str) {
        System.out.println(str);
    }

    public void tiskni() {
        System.out.println(str);
    }

    @Override
    public void accept(Dvojtecky dvojtecky) {
        System.out.println("accept: " + dvojtecky.str);
    }
}

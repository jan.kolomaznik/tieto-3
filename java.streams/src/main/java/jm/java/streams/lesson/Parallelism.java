package jm.java.streams.lesson;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Examples of using parallel streams.
 */
public final class Parallelism {

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        // Let's have a generator for later and use it for making random numbers
        // to be sorted in various ways
        final Random random = new Random();
        final Supplier<Integer> randomInts = () -> random.nextInt(100);
        // Here we can see another predefined collector: toList() makes a list
        final List<Integer> unsorted = Stream.generate(randomInts).limit(10).collect(Collectors.toList());
        System.out.println(unsorted);

        // We've seen sorting before
        System.out.println(unsorted.stream().sorted(Integer::compare).collect(Collectors.toList()));
        // To become parallel it is enough to get a parallel stream
        System.out.println(unsorted
                .parallelStream()
                .sorted(Integer::compare)
                .collect(Collectors.toList()));

        // Or when we don't have a collection, just a generator...
        System.out.println(Stream.generate(randomInts).limit(10).parallel() // This is the magic switch!
                .sorted(Integer::compare).collect(Collectors.toList()));

        // OK, let's assume we already have a list and we want to add to it
        final List<Integer> sorted = new ArrayList<>();
        unsorted.parallelStream().sorted(Integer::compare).forEach(sorted::add);
        System.out.println(sorted);

        // But wait... this didn't produce what we expected!
        //
        // The explanation is that some streams needn't be ordered. It is not
        // a problem for some operations, but others may require ordering or
        // stream stability.
        //
        // The predefined Collections.toList() collector requires ordering, so
        // it enforces keeping the sorted order. But forEach() does not do that.
        // Rather forEachOrdered() must be used.
        sorted.clear();
        unsorted.parallelStream().sorted(Integer::compare).forEachOrdered(sorted::add);
        System.out.println(sorted);

        // Anyway, ordering or stability on parallel streams may quite expensive.
        // Try to limit ordering requirements or group the operations in the
        // most effective way with the respect to their nature. It is possible
        // switch to parallel streams (we've seen it) and back:
        sorted.clear();
        unsorted.parallelStream().sorted(Integer::compare).sequential().forEach(sorted::add);
        System.out.println(sorted);
    }
}

[remark]:<class>(center, middle)
# Síťové operace
## Spojovaná komunikace prostřednictvím protokolu TCP (spolehlivá)

[remark]:<slide>(new)
## TCP klient
* Implementace spojově orientované komunikace je v Javě jednodušší, než paketové. 

* Pracuje se totiž se streamy – tedy stejně, jako při práci se soubory.

```java
Socket sock = new Socket("akela.mendelu.cz", 80); // připojení

BufferedReader br = new BufferedReader(new InputStreamReader(sock.getInputStream()));
BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));

bw.write(request);  // zapíšeme předem připravený požadavek
bw.flush();         // vyprázdnění (tzn. odeslání) bufferu

String line = br.readLine();

// dokud jsou data, opakuj
while (line != null) {
   System.out.println(line);  // platná data vypisuj
   line = br.readLine();
}

sock.close(); // zavření socketu
```

[remark]:<slide>(new)
## TCP server
* Typické chování serveru: čeká na připojení klienta, a až ten se připojí, obslouží ho – a současně čeká na připojení případných dalších klientů.

* Vytvoříme instanci třídy `ServerSocket` a zde metodou `accept()` nasloucháme na příslušném portu. 
  - V okamžiku, kdy se připojí klient, metoda vrátí instanci třídy Socket. 
  - Z ní, úplně stejně jako na klientské straně, získáme vstupní a výstupní stream, a přes tyto streamy již normálně komunikujeme.

* Čekání na další klienty během obsluhy jednoho klienta lze zajistit pomocí vláken. 
  Vytvoříme nové vlákno, které obsluhuje klienta, zatímco původní vlákno opět zavolá metodu `accept()` a čeká na další připojení:

[remark]:<slide>(new)
### TCP server: příklad
```java
boolean quit = false;
   
try {
   ServerSocket ss = new ServerSocket(22222);

   while (!quit) {
       final Socket sock = ss.accept();
       Thread t = new Thread() {
           public void run() {
               try {
                   InputStream is = sock.getInputStream();
                   OutputStream os = sock.getOutputStream();
                   ...
                   sock.close();
               } catch (IOException e) {
                   ...
               }
           }
       };
       t.setDaemon(true);
       t.start();
   }
} catch (Exception e) {
   ...
}
```
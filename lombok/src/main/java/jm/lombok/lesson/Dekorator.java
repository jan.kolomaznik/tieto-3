package jm.lombok.lesson;

import lombok.RequiredArgsConstructor;
import lombok.experimental.Delegate;

public class Dekorator {

    @RequiredArgsConstructor
    static class AdapterImpl implements Adapter {

        @Delegate
        private final Adaptee instance;

    }

    interface Adapter {
        void display();
    }

    static class Adaptee {
        public void display() {
            System.out.println("In Adaptee.display()");
        }
    }

    public static void main(String[] args) {
        AdapterImpl impl = new AdapterImpl(new Adaptee());
        impl.display();
    }

}

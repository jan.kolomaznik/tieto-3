package jm.lombok.lesson;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import java.time.LocalDate;


@AllArgsConstructor
public class PersonLombok_1 {

    @Getter
    private String firstName;

    @Getter
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        new PersonLombok_1(null, "Kumar", LocalDate.now());
    }
}

package jm.lombok.lesson;

import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogEmployee {

    public void work(){
        log.info("Log method");
    }

    public static void main(String[] args) {
        LogEmployee employee = new LogEmployee();
        employee.work();
    }
}

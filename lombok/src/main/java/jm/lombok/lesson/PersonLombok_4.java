package jm.lombok.lesson;

import lombok.Value;
import lombok.experimental.Wither;

import java.time.LocalDate;

@Value
@Wither
public class PersonLombok_4 {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        PersonLombok_4 person = new PersonLombok_4("Pepa", "Zdepa", null);
        PersonLombok_4 personWith = person
                .withFirstName("Honza")
                .withDateOfBirth(LocalDate.now());
        System.out.println(person);
        System.out.println(personWith);
    }
}


package jm.lombok.lesson;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

public class PersonLombok_2 {

    private Long id;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        //PersonLombok_2 person = new PersonLombok_2("Pepa", "Zdepa", LocalDate.now());
        //person.setFirstName("Tomas");
        //System.out.println("firstName: " + person.getFirstName());
        //System.out.println("lastName: " + person.getLastName());
    }
}


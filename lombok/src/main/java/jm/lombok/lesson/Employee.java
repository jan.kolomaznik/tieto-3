package jm.lombok.lesson;

import lombok.*;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Employee {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        Employee person = Employee.builder()
                .firstName("Pepa")
                .lastName("Zdepa")
                .build();
        System.out.println(person);
    }
}

package cz.ictpro.java.design.patterns;

import javax.jws.soap.SOAPBinding;
import java.util.*;

public class Mediator {

    public static class ChatRoom {
        private static Map<String, Set<User>> channels = new HashMap<>();

        public static void registr(String channel, User user) {
            if (!channels.containsKey(channel)) {
                channels.put(channel, new HashSet<>());
            }
            channels.get(channel).add(user);
        }

        public static void showMessage(User user, String message){
            System.out.println(new Date().toString() + " [" +
                    user.getName() + "] : " + message);
            if (channels.containsKey(user.getName())) {
                for (User receiver: channels.get(user.getName())) {
                    receiver.receiveMessage(user, message);
                }
            }
        }
    }

    public static class User {
        private String name;
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public User(String name){
            this.name  = name;
        }
        public void sendMessage(String message){
            ChatRoom.showMessage(this,message);
        }
        public void receiveMessage(User form, String message) {
            System.out.format("- User %s receive form %s message %s\n", name, form.name, message);
        }
    }

    public static void main(String[] args) {
        User robert = new User("Robert");
        User john = new User("John");
        ChatRoom.registr("John", robert);
        ChatRoom.registr("Robert", john);
        robert.sendMessage("Hi! John!");
        john.sendMessage("Hello! Robert!");
    }
}

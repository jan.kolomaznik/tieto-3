package jm.java.collections.exercise
/**
 * Created by xkoloma1 on 02.11.2016.
 */
class Ukol_3_Test extends GroovyTestCase {

    public void "test opakovaneho pridani"() {
        Predmet predmet = new Predmet("PJJ");

        assert predmet.zapisDoPredmetu(new Student("Tomaš", "Novak")) == true;
        assert predmet.zapisDoPredmetu(new Student("Tomaš", "Novak")) == false;
    }

    public void "test serazeni studentu v predmetu"() {
        Predmet predmet = new Predmet("PJJ");
        predmet.zapisDoPredmetu(new Student("Tomaš", "Novak"));
        predmet.zapisDoPredmetu(new Student("Jan", "Vopička"));
        predmet.zapisDoPredmetu(new Student("Jan", "Adamek"));
        predmet.zapisDoPredmetu(new Student("Tomaš", "Novak"));

        Iterator<Student> studentIterator = predmet.getStudents().iterator();

        assert studentIterator.next().equals(new Student("Jan", "Adamek"));
        assert studentIterator.next().equals(new Student("Tomaš", "Novak"));
        assert studentIterator.next().equals(new Student("Jan", "Vopička"));
        assert studentIterator.hasNext() == false;
    }

    public void "test implementace metody 'compateTo(Student o)'"() {
        Student a = new Student("Tomaš", "Novak");
        Student b = new Student("Jan", "Vopička");
        Student c = new Student("Jan", "Adamek");
        Student d = new Student("Tomaš", "Novak");

        assert a.compareTo(b) < 0
        assert b.compareTo(c) > 0
        assert a.compareTo(d) == 0
    }
}

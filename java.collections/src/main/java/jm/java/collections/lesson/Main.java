package jm.java.collections.lesson;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        Map<String, Integer> mapa = new HashMap<>();
        mapa.put("A", 1);
        mapa.put("B", null);


        for (String key: mapa.keySet()) {
            Integer value = mapa.get(key);
            System.out.format("%s: %s\n", key, value);
        }

        for (Map.Entry<String, Integer> entry: mapa.entrySet()) {
            System.out.format("%s: %s\n", entry.getKey(), entry.getValue());

        }

    }

}

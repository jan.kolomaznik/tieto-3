package jm.java.collections.exercise;

import java.util.Objects;

/**
 * Created by xkoloma1 on 02.11.2016.
 */
public class Student implements Comparable<Student> {

    private final String jmeno;
    private final String prijmeni;

    public Student(String jmeno, String prijmeni) {
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(jmeno, student.jmeno) &&
                Objects.equals(prijmeni, student.prijmeni);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jmeno, prijmeni);
    }

    @Override
    public String toString() {
        return "Student{" +
                "jmeno='" + jmeno + '\'' +
                ", prijmeni='" + prijmeni + '\'' +
                '}';
    }

    @Override
    public int compareTo(Student other) {
        int result = this.prijmeni.compareTo(other.prijmeni);
        if (result != 0) {
            return result;
        }

        return this.jmeno.compareTo(other.jmeno);

    }
}

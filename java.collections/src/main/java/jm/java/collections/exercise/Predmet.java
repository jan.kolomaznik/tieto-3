package jm.java.collections.exercise;

import javax.swing.text.StyledEditorKit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by xkoloma1 on 02.11.2016.
 */
public class Predmet {

    private final String kod;

    private List<Cviceni> cviceniList = new ArrayList<>();
    private SortedSet<Student> studentSet = new TreeSet<>();
    //private Map<Student, Cviceni> studentMap = new TreeMap<>();
   
    public Predmet(String kod) {
        this.kod = kod;
    }

    public Cviceni zalozCviceni(int kapacita) {
        Cviceni newCviceni = new Cviceni(kapacita, this);
        cviceniList.add(newCviceni);
        return newCviceni;
    }

    public Collection<Cviceni> getCvicenis() {
        return Collections.unmodifiableList(cviceniList);
    }

    public Cviceni getCviceni(int index) {
        return cviceniList.get(index);
    }

    public boolean zapisDoPredmetu(Student student) {
        return studentSet.add(student);
    }

    public Collection<Student> getStudents() {
        return Collections.unmodifiableSet(studentSet);
    }

    public Collection<Student> getStudentyBezCviceni() {
        Set<Student> stdentiVeCviceni = cviceniList.stream()
                .flatMap(c -> c.getStudents().stream())
                .collect(Collectors.toSet());
        return studentSet.stream()
                .filter(s -> !stdentiVeCviceni.contains(s))
                .collect(Collectors.toSet());
    }

}

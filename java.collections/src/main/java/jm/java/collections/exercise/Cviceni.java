package jm.java.collections.exercise;


import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by xkoloma1 on 02.11.2016.
 */
public class Cviceni {

    private final int kapacita;

    private final Predmet predmet;

    private Set<Student> studentSet = new HashSet<>();

    public Cviceni(int kapacita) {
        this(kapacita, null);
    }

    Cviceni(int kapacita, Predmet predmet) {
        this.kapacita = kapacita;
        this.predmet = predmet;
    }

    public boolean prihlasit(Student student) {
        if (predmet != null) {
            if (!testStudentSplnujePodminkyPredmetuProZapisDoCviceni(student)) {
                return false;
            }
        }

        if (studentSet.size() >= kapacita) {
            return false;
        }

        return studentSet.add(student);
    }

    public int getKapacita() {
        return kapacita;
    }

    private boolean testStudentSplnujePodminkyPredmetuProZapisDoCviceni(Student student) {
        return predmet.getStudents().contains(student) && predmet.getStudentyBezCviceni().contains(student);
    }

    public Collection<Student> getStudents() {
        return Collections.unmodifiableSet(studentSet);
    }

    @Override
    public String toString() {
        return "Cviceni{" +
                "kapacita=" + kapacita + ", " +
                "studens= " + studentSet.size() +
                '}';
    }
}

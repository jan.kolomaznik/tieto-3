package jm.java.collections.exercise;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class Registrace {

    private Predmet predmet;

    private Queue<Student> studentQueue = new LinkedList<>();

    public Registrace(Predmet predmet) {
        this.predmet = predmet;
    }

    public void zaregistruj(Student student) {
        if (studentQueue.contains(student)) {
            return;
        }
        studentQueue.add(student);
    }

    public Queue<Student> getStudents() {
        return studentQueue;
    }

    public int hromadnyZapis() {
        int registrovanych = studentQueue.size();
        int kapacita = predmet.getCvicenis().stream()
                    .mapToInt(Cviceni::getKapacita)
                    .sum();

        for (int i = 0; i < Math.min(registrovanych, kapacita); i++) {
            predmet.zapisDoPredmetu(studentQueue.remove());
        }
        return kapacita - registrovanych;

    }
}
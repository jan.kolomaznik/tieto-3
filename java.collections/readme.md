[remark]:<class>(center, middle)
# Java Collections API

[remark]:<slide>(new)
## Kontejnery
* Kontejnery slouží k ukládání objektů (ne hodnot primitivních typů!).
* Jsou dynamickými alternativami k polím, oproti nimž mají řadu výhod, např.:

  * proměnnou velikost,
  * přístup nejen podle číselného indexu,
  * možnost manipulace s objekty (řazení, zjišťování přítomnosti prvku apod.).

* Většinou se používají kontejnery hotové, vestavěné, tj. ty, jež jsou součásti standardní knihovny Java Core API. 

  * Kontejnerové třídy najdeme v balíku `java.util`.
  

[remark]:<slide>(new)
### Kontejnery a generika
  
[remark]:<slide>(new)
### Základní typy kontejnerů
* Jsou tři:
  - **Množiny** (implementují rozhraní `java.util.Set`). 
    Prvek může být v množině nejvýš jednou, nemá v ní určenou polohu. 
    Lze se rychle dotazovat na přítomnost prvku v množině.

  - **Seznamy** (implementují rozhraní `java.util.List`). 
    Prvek může být v kontejneru víckrát a má určenou jednoznačnou polohu.

  - **Asociativní pole** (mapy) (implementují rozhraní `java.util.Map`). 
    Struktura uchovávající dvojice klíč–hodnota, rychlý přístup přes klíč.
    
* Množiny a seznamy implementují rozhraní `java.util.Collection` a řadí se tak mezi tzv. kolekce.

[remark]:<slide>(new)
## Iterátory
* Iterátory zajišťují sekvenční přístup k prvkům kolekcí, a to:
  - v neurčeném pořadí, nebo
  - v určeném pořadí (u uspořádaných kolekcí).
  
* Každý iterátor musí implementovat velmi jednoduché rozhraní `java.util.Iterator` se třemi metodami:
  - **boolean hasNext()** – vrací `true`, pokud je iterátor schopen poskytnout další prvek kolekce (v tom případě metoda `next()` vrátí objekt). 
    Pokud `hasNext()` vrátí `false`, pak metoda `next()` vyhodí výjimku `NoSuchElementException`.
  - **Object next()** – vrací následující prvek kolekce.
  - **void remove()** – odstraní z kolekce prvek naposledy vrácený metodou `next()`.

* Iterátor k dané kolekci (tj. množině nebo seznamu) získáme pomocí metody `Iterator iterator()` dané kolekce.

[remark]:<slide>(new)
## Kolekce
* jsou kontejnery implementující rozhraní `java.util.Collection`.

* Rozhraní kolekce popisuje velmi obecný kontejner, disponující operacemi: přidávání, rušení prvku, získání iterátoru, zjišťování prázdnosti atd.

![](media/InheritanceTree.jpg)

[remark]:<slide>(new)
### Seznamy
* Lineární struktury.

* Implementují rozhraní `java.util.List` (rozšíření `java.util.Collection`).

* Prvky lze adresovat indexem (typu int).

* Poskytují možnost získat dopředný i zpětný iterátor (pomocí metody `ListIterator listIterator()`).

* Lze pracovat i s podseznamy (získáme metodou `List subList(int fromIndex, int toIndex)`).

* Nejpoužívanějším seznamem je třída `java.util.ArrayList`. 
  - Nejvíce připomíná klasické pole, ovšem s proměnnou délkou. 
  - Pro přístup k jednotlivým prvkům lze používat indexy, protože prvky jsou udržovány v určitém pořadí. 
  - Může obsahovat stejné (duplicitní) prvky.
  
[remark]:<slide>(new)
### Seznamy 2
* Třída `java.util.LinkedList` představuje spojový seznam prvků s možností přidávat/odebírat prvky na začátku nebo konci seznamu.

* `java.util.LinkedList` lze použít jako zásobník nebo frontu
  - **Fronta**: `Queue<E>` 
  - **Zasobnik** (Double-end-fronta): `Dequeue<E>`

* Třída `java.util.Vector` je součástí standardní knihovny již od verze 1.0 jazyka. 
  - Dnes se místo něj používá `java.util.ArrayList`. 

[remark]:<slide>(new)
### Množiny
* Jsou struktury standardně bez uspořádání prvků (ale existují i uspořádané, viz níže).

* Implementují rozhraní `java.util.Set` (což je rozšíření `java.util.Collection`).

* Cílem množin je mít možnost rychle provádět atomické operace:
  - vkládání prvku (`boolean add(Object)`),
  - odebírání prvku (`boolean remove(Object`)),
  - dotaz na přítomnost prvku (`boolean contains(Object)`).

[remark]:<slide>(new)
#### Množiny neuspořádané
```java
public class Clovek {
   private final String name;
    
   public Clovek(String j) {
      name = j;    
   }
    
   protected String getName() {
      return name;
   }
}
```
```java
public class Neusporadana {
   public static void main(String[] args) {
      Set mnozina = new HashSet();
      Clovek c1 = new Clovek("Karel");
      Clovek c2 = new Clovek("Eva");
      Clovek c3 = new Clovek("Petr");
      mnozina.add(c1);
      mnozina.add(c2);
      mnozina.add(c3);
      System.out.println("Je v mnozine Eva?" + mnozina.contains(c2));
      Iterator i = mnozina.iterator();
      while(i.hasNext()) {
         System.out.println(((Clovek)i.next()).getName());
      }
   }
}	
```

[remark]:<slide>(new)
### Uspořádané množiny
* Implementují rozhraní `java.util.SortedSet`.

* Třída `java.util.TreeSet`. 
  - V `TreeSet` se průběžně udržují prvky seřazené (využívá stromovou strukturu). 
  - Již v okamžiku vložení se vkládaný prvek zařadí do odpovídajícího pořadí vzhledem ke stávajícím prvkům. 
  - Vkládání do `TreeSet` je tak pomalejší, než do `HashSet`. 
  - Kupodivu pomalejší je i jakákoliv další práce s `TreeSet`, kromě vyhledávání prvku (kde však rozdíl rychlostí není významný).

* Jednotlivé prvky lze tedy iterátorem procházet v přesně definovaném pořadí – uspořádání podle hodnot prvků.

* Uspořádání je dáno buďto:
  - standardním chováním metody `int compareTo(Object)` vkládaných objektů, pokud implementují rozhraní `java.lang.Comparable`
  - nebo je možné uspořádání definovat pomocí tzv. komparátoru (objektu impl. rozhraní `Comparator`) poskytnutého při vytvoření množiny.

[remark]:<slide>(new)
#### Uspořádané množiny - implemntace rozhraní Comparable
```java
public class ClovekComp implements Comparable {
   private final String name;
    
   public ClovekComp(String j) {
      name = j;    
   }
    
   protected String getName() {
      return name;
   }
    
   public int compareTo(Object o) {
      if (o instanceof ClovekComp) {
         ClovekComp c = (ClovekComp) o;
         return name.compareTo(c.getName()); // porovnáváme lidi jen podle jména
      }
      else
         throw new IllegalArgumentException("Nelze porovnat.");
   }
}
```

[remark]:<slide>(new)
#### Uspořádané množiny - implemntace rozhraní Comparable
```java
public class Mnoziny2 {
    
   public static void main(String[] args) {
      SortedSet mnozina = new TreeSet();
      ClovekComp c1 = new ClovekComp("Karel");
      ClovekComp c2 = new ClovekComp("Eva");
      ClovekComp c3 = new ClovekComp("Petr");
      mnozina.add(c1);
      mnozina.add(c2);
      mnozina.add(c3);
      Iterator i = mnozina.iterator();
      while(i.hasNext()) {
         System.out.println(((ClovekComp)i.next()).getName());
      }
   }
}
```

[remark]:<slide>(new)
#### Uspořádané množiny - implemntace rozhraní Comparator
```java
class ClComparator implements Comparator {
   public int compare(Object o1, Object o2) {
      // porovnává lidi jen podle jména
      if (o1 instanceof Clovek && o2 instanceof Clovek) {
         Clovek c1 = (Clovek)o1;
         Clovek c2 = (Clovek)o2;
         return c1.getName().compareTo(c2.getName());
      } else
         throw new IllegalArgumentException("Nelze porovnat.");
   }
}
```

[remark]:<slide>(new)
#### Uspořádané množiny - implemntace rozhraní Comparator
```java
import java.util.*;

public class Mnoziny {
    
   public static void main(String[] args) {
      SortedSet mnozina = new TreeSet(new ClComparator());
      Clovek c1 = new Clovek("Karel");
      Clovek c2 = new Clovek("Eva");
      Clovek c3 = new Clovek("Petr");
      mnozina.add(c1);
      mnozina.add(c2);
      mnozina.add(c3);
      Iterator i = mnozina.iterator();
      while(i.hasNext()) {
         System.out.println(((Clovek)i.next()).getName());
      }
   }
}
```

[remark]:<slide>(new)
### Další možnosti setřízení kolkcí

* Kolekci lze seřadit i následně (musí samozřejmě obsahovat porovnatelné objekty) statickou metodou `sort(List list)` třídy `java.util.Collections`. 
  - (Pozor, neplést s rozhraním Collection!)
  
```java  
List seznam = new ArrayList();
seznam.add("Petr");
seznam.add("Standa");
seznam.add("Jirka");
Collections.sort(seznam);
for (Object o : seznam) {
   System.out.println(o);
}
```

* vypíše

```java
Jirka
Petr
Standa
```
    
[remark]:<slide>(new)
### Mapy
* Mapy (asociativní pole, nepřesně také hašovací tabulky nebo haše) fungují v podstatě na stejných principech a požadavcích jako `Set`.

* Ukládají ovšem dvojice (klíč, hodnota) a umožnují rychlé vyhledání dvojice podle hodnoty klíče.

* Pomocí klíče, který je neměnný a unikátní, se vyhledává hodnota. 
  - Hodnota je proměnná a může být duplicitní, tj. dva různé klíče mohou mít stejnou hodnotu.

* Základními metodami jsou:
  - vložení prvku: `put(key, value)`,
  - odstranění prvku podle hodnoty klíče: `remove(key)`,
  - dotaz na přítomnost klíče v mapě: `boolean containsKey(key)`,
  - dotaz na přítomnost hodnoty v mapě: `boolean containsValue(value)`,
  - výběr hodnoty odpovídající zadanému klíči: `Object get(key)`.

[remark]:<slide>(new)
### Mapy - implementace
* Nejpoužívanější třídou je zde HashMap.

![](media/map.gif)

[remark]:<slide>(new)
### Mapy - TreeMap
* V `TreeMap` jsou jednotlivé prvky seřazeny podle hodnoty klíče. 
  - `TreeMap` se používá (stejně jako `TreeSet`) méně, když potřebujeme mít prvky seřazené. 
  - Seřazení klíčů je nezbytné v tom případě, kdy potřebujeme získat z mapy: největší či nejmenší klíč, „podmapu“ v závislosti na hodnotě klíče.

* TreeMap obsahuje všechny metody ze třídy HashMap a přidává k nim ještě metody:
 - `key firstKey()` – vrací nejmenší (první v pořadí) klíč,
 - `key lastKey()` – vrací největší (poslední v pořadí) klíč.

[remark]:<slide>(new)
### Výčtový typ a datové struktury
* Pro použití výčtových typů v množinách či mapách, poskytuje Java v balíčku `java.util` speciální rychlé implementace množiny a mapy – třídy `EnumSet` a `EnumMap`.
  
* Kolekce `EnumMap` představuje mapu, jejímiž klíči jsou konstanty nějakého výčtového typu. 
  - Hodnotami mohou být libovolné objekty.
  
#### Notes
TODO slide o hashCode a Eq...
TODO slode o tom, co se děje, když se to meni
  
  


[remark]:<slide>(new)
## Porovnatelnost objektů
* Porovnatelnost objektů je důležitá vlastnost, kterou potřebujeme pro uložení do některých kontejnerů.

* Zjišťujeme, zda jsou objekty sobě rovny, nebo je jeden z nich „větší“ či „menší“ než druhý (u číselných hodnot je význam jasný; ostatní datové objekty mohou mít porovnatelnost implementovánu prakticky libovolným způsobem). 

* V Javě je porovnatelnost zaručena tím, že třída implementuje rozhraní `Comparable` (obsahuje jedinou metodu `compareTo()`).

* Většina standardních tříd rozhraní Comparable neimplementuje. 
  - Můžeme však vytvořit komparátor (rozhraní Comparator), který lze použít neomezeně, s libovolnými objekty (záleží pouze na jeho implementaci). 
  - Komparátor zajišťuje úplné uspořádání objektů (pomocí operací porovnání a testu rovnosti), můžeme ho použít jak při explicitním řazení prvků, tak ho předat některým kolekcím pro použití k vnitřnímu uspořádání.

[remark]:<slide>(new)
#### Příklad porovnávánní objektů

```java
Set mnozina = new HashSet();
Clovek c1 = new Clovek("Eva");
Clovek c2 = new Clovek("Eva");
Clovek c3 = new Clovek("Eva");
mnozina.add(c1);
mnozina.add(c2);
mnozina.add(c3);
```

* Výše uvedená množina bude obsahovat 3 prvky.

[remark]:<slide>(wait)
#### Příklad porovnání String

```java
Set mnozina = new HashSet();
String c1 = "Eva";
String c2 = "Eva";
String c3 = "Eva";
mnozina.add(c1);
mnozina.add(c2);
mnozina.add(c3);
```

* Výše uvedená množina bude obsahovat pouze 1 prvek. Proč?

[remark]:<slide>(new)
## Wrappery pro práci s kolekcemi
* Wrappery jsou objekty, které se „předřadí“ před jiné objekty, převezmou navenek jejich chování a přidají, odeberou či změní právě to, co je třeba.

[remark]:<slide>(wait)
### Synchronizační wrappery
* Všechny standardní implementace kolekcí jsou bez synchronizace vícenásobného přístupu k objektu, takže se ve vícevláknovém prostředí může stát, že k témuž objektu přistupuje více vláken současně a dojde k přečtení nekonzistentních dat nebo k poškození objektu.

* Máme-li záruku, že ke kolizi nemůže dojít (program je pouze jednovláknový), není třeba zajišťovat synchronizaci. 
  - Na tento předpoklad ale nemůžeme prakticky nikdy spoléhat 
  - musíme synchronizaci zajistit buď explicitně (což je pracnější) nebo použít synchronizační wrapper. 
  - Netýká se to „starých“ kontejnerů `Vector` a `Hashtable`, ty mají synchronizaci již v sobě.

[remark]:<slide>(new)
### Synchronizační wrappery
* Wrapper funguje tak, že příslušné statické metodě z třídy `Collections`(pozor, neplést s rozhraním Collection!) předáme původní objekt a metoda nám vrátí wrapper k tomuto objektu, který už pak používáme stejně jako původní objekt.

```java
List l = Collections.synchronizedList(new LinkedList());

// pro speciální použití - ponecháváme si i přístup k původní instanci
SortedSet s1 = new TreeSet();
SortedSet s2 = Collections.synchronizedSortedSet(s1);
```

[remark]:<slide>(new)
### Wrappery pro zákaz modifikací
* Někdy je třeba zakázat jakékoliv změny v kontejneru. Nejčastěji tehdy, když z nějakého uceleného systému (který se stará o přípravu dat) předáváme někam ven referenci na kontejner a nechceme, aby ho někdo zvenku měnil.

* Způsob vytvoření wrapperu je obdobný jako v případě synchonizačního wrapperu (viz příklad níže).
  - Pokus o modifikaci wrapperového kontejneru způsobí výjimku `UnsupportedOperationException`. 
  - Původní kontejner samozřejmě lze (přes referenci na něj) i nadále měnit.

```java
// vytvoření seznamu
List lst = new ArrayList();

// příprava dat apod.

// nyní se vytvoří neměnná kolekce
Collection c = Collections.unmodifiableCollection(lst);

// pokus o změnu - vyvolá výjimku UnsupportedOperationException
c.add(new Object());
```

[remark]:<slide>(new)
[remark]:<class>(center, middle)
# Samostatný úkol

[remark]:<slide>(new)
## 1. Přípravná fáze

Otevřete projekt v tématu (pomocí `build.gradle`)

[remark]:<slide>(wait)
Ten Obsahuje: 

1. Třídu `Student`, která obsahuje

  - Bude obsahovat `String jmeno` a `String prijmeni`, tyto atributy každého studenta jednoznačně identifikují

2. Třídu `Cviceni`, která obsahuje

  - Bude obsahovat atribut `int kapacita`

3. Třídu `Predmet`, která obsahuje

  - Bude jednoznačně identifikována atributem `String kod`

4. Třídu `Registrace`, která obsahuje

    - Registrace je vždy vypsána pro nějaký předmět

[remark]:<slide>(new)
#### 1. Úkol
Vytvořte metody objektu `Cviceni`, které 

- Umožní přihlásit studenta do cvičení, pokud to umožní kapacita cvičení: `boolean prihlasit(Student student)`

- Vrátí kolekci zapsaných studentů: `Collection<Student> getStudents()`

[remark]:<slide>(new)
#### 2. Úkol
Vytvořte metody objektu `Predmet`, které:

- Umožní v rámci předmětu vytvořit cvičení (cvičení jsou v rámci předmětu jednoznačně určena svým indexem): `Cviceni zalozCviceni(int kapacita)`

- Vrátí kolekci cvičení `Collection<Cviceni> getCvicenis()`

- Vrátí Cvičení: `Cviceni getCviceni(int index)`
  
[remark]:<slide>(new) 
#### 3. Úkol
Vytvořte metody objektu `Predmet`, které

- Umožní zapsat studenta do předmětu: `boolean zapisDoPredmetu(student)`

- Vrátí abecedně setřesenou kolekci všech zapsaných studentů: `Collection<Student> getStudents()`

[remark]:<slide>(new) 
#### 4. Úkol
Vytvořte metody objektu Predmet a Cvičení tak aby:

- Umožní přihlásit studenta do cvičení, pokud je zapsán v předmětu a v rámci předmětu se může student přihlásit pouze do jednoho cvičení.

- Vypíše všechny studenty, kteří nejsou zapsáni do žádného cvičení: `Collection<Student> getStudentyBezCviceni()`

[remark]:<slide>(new) 
#### 5. Úkol
Vytvořte metody objektu Registrace, které

- Metodu `void zaregistruj(student)` pro registraci studenta k předmětu, každý student může být zaregistrován jen jednou. 

- Vypíše všechny studenty, kteří jsou zaregistrováni k předmětu v pořadí jak se registrovali: `Queue<Student> getStudents()`

- Vytvořte metodu `int hromadnyZapis()`, která zapíse tolik studentů, kolik je kapacita všech cvičení v předmětu. 
  Metoda vratí zbylou kapacitu cvičení, což může být i záporné číslo v případě, že se nepodařilo zaregistrovat všechny studenty.


package cz.ictpro.java.design.patterns;

public class Adapter {

    public static void main(String[] args) {
        Shape shape = new Shape();
        Drawable drawable = new ShapeToDrawable(shape);
        display(drawable);
    }
    public static void display(Drawable drawable) {
        drawable.draw(1,1,2,3);
    }
    public static class Shape {
        public void display(int x1, int y1, int x2, int y2) {
            System.out.format("Draw [%d, %d] - [%d, %d]", x1, y1, x2, y2);
        }
    }
    public interface Drawable {
        public void draw(int x,int y,int w,int h);
    }
    public static class ShapeToDrawable implements Drawable {
        private final Shape shape;
        public ShapeToDrawable(Shape shape) {
            this.shape = shape;
        }
        public void draw(int x, int y, int w, int h) {
            shape.display(x, y, x + w, y + h);
        }
    }
}

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Grep {

    public static void main(String[] args) throws IOException {
        Path root = Paths.get(args[0]);
        Pattern pattern = Pattern.compile(args[1]);

        System.out.println(root.toAbsolutePath());
        System.out.println(pattern);

        Files.walk(root)
                .filter(path -> Files.isRegularFile(path))
                .filter(Grep::binFile)
                .peek(System.out::println)
                .flatMap(Grep::lines)
                .filter(pattern.asPredicate())
                .forEach(Grep::print);
    }

    private static boolean binFile(Path path) {
        try {
            char[] buf = new char[64];
            Files.newBufferedReader(path).read(buf);
            return true;
        } catch (IOException e) {
            return false;
        }

    }

    private static Stream<String> lines(Path path) {
        try {
            return Files.lines(path);
        } catch (Exception e) {
            e.printStackTrace();
            return Stream.empty();
        }
    }

    private static void print(String line) {
        try {
            System.out.println(line);
        } catch (Exception e) {
            System.out.println("BIN");
        }
    }
}

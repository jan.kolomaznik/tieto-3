package cz.mendelu.swi.morse;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringReader;

import org.junit.Test;

public class MorseReaderTest {

	@Test
	public void testSimpleLetter() throws IOException {
		MorseReader reader = new MorseReader(new StringReader(".-"));
		assertEquals('A', (char)reader.read());
		assertEquals(-1, reader.read());
	}
	
	@Test
	public void testMultiLetters() throws IOException {
		MorseReader reader = new MorseReader(new StringReader(".-|-..."));
		assertEquals('A', (char)reader.read());
		assertEquals('B', (char)reader.read());
		assertEquals(-1, reader.read());
	}
	
	@Test
	public void testTwoWords() throws IOException {
		MorseReader reader = new MorseReader(new StringReader(".-||-..."));
		assertEquals('A', (char)reader.read());
		assertEquals(' ', (char)reader.read());
		assertEquals('B', (char)reader.read());
		assertEquals(-1, reader.read());
	}
	
	@Test
	public void testTwoSentens() throws IOException {
		MorseReader reader = new MorseReader(new StringReader(".-|||-..."));
		assertEquals('A', (char)reader.read());
		assertEquals('.', (char)reader.read());
		assertEquals('B', (char)reader.read());
		assertEquals(-1, reader.read());
	}

}

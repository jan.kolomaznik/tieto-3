package cz.mendelu.swi.morse;

import java.io.*;

public class Demo {

    public static void main(String[] args) {
        File messageFile = new File("message.txt");

        try (MorseWriter mw = new MorseWriter(
                new FileWriter(messageFile))
        ) {
            mw.write("Hello world");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader br = new BufferedReader(
                new FileReader(messageFile))
        ) {
            br.lines().forEach(System.out::println);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader br = new BufferedReader(
                new MorseReader(
                        new FileReader(messageFile)))
        ) {
            br.lines().forEach(System.out::println);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

package cz.mendelu.swi.morse;

import java.io.IOException;
import java.io.Writer;

public class MorseWriter extends Writer {

	private final Writer deroratedWriter;

	private final MorseCode morseCode;


	public MorseWriter(Writer out) {
		this.deroratedWriter = out;
		this.morseCode = new MorseCode();
	}

	@Override
	public void write(char[] cbuf, int off, int len) throws IOException {
		throw new UnsupportedOperationException("TODO");
	}

	@Override
	public void flush() throws IOException {
		throw new UnsupportedOperationException("TODO");

	}

	@Override
	public void close() throws IOException {
		throw new UnsupportedOperationException("TODO");
	}
}

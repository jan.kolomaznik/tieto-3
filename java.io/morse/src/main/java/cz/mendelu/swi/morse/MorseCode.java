package cz.mendelu.swi.morse;

import java.util.concurrent.ScheduledExecutorService;

public class MorseCode {

	private final static String SEPARATOR = "|";
	
	public String getLetterSeperator() {
		return SEPARATOR;
	}
	
	public String getWorldSeperator() {
		return SEPARATOR + SEPARATOR;
	}
	
	public String getSentensSeperator() {
		return SEPARATOR + SEPARATOR + SEPARATOR;
	}

	public String encode(char c) {
		c = Character.toUpperCase(c);
		switch (c) {
		case 'A':
			return ".-";
		case 'B':
			return "-...";
		case 'C':
			return "-.-.";
		case 'D':
			return "-..";
		case 'E':
			return ".";
		case 'F':
			return "..-.";
		case 'G':
			return "--.";
		case 'H':
			return "....";
		case 'I':
			return "..";
		case 'J':
			return ".---";
		case 'K':
			return "-.-";
		case 'L':
			return ".-..";
		case 'M':
			return "--";
		case 'N':
			return "-.";
		case 'O':
			return "---";
		case 'P':
			return ".--.";
		case 'Q':
			return "--.-";
		case 'R':
			return ".-.";
		case 'S':
			return "...";
		case 'T':
			return "-";
		case 'U':
			return "..-";
		case 'V':
			return "...-";
		case 'W':
			return ".--";
		case 'X':
			return "-..-";
		case 'Y':
			return "-.--";
		case 'Z':
			return "--..";
		default:
			throw new IllegalArgumentException(String.format(
					"Neplatný morse kód: '%1$s'", c));
		}
	}

	public int decode(String code) {
		switch (code) {
		case ".-":
			return 'A';
		case "-...":
			return 'B';
		case "-.-.":
			return 'C';
		case "-..":
			return 'D';
		case ".":
			return 'E';
		case "..-.":
			return 'F';
		case "--.":
			return 'G';
		case "....":
			return 'H';
		case "..":
			return 'I';
		case ".---":
			return 'J';
		case "-.-":
			return 'K';
		case ".-..":
			return 'L';
		case "--":
			return 'M';
		case "-.":
			return 'N';
		case "---":
			return 'O';
		case ".--.":
			return 'P';
		case "--.-":
			return 'Q';
		case ".-.":
			return 'R';
		case "...":
			return 'S';
		case "-":
			return 'T';
		case "..-":
			return 'U';
		case "...-":
			return 'V';
		case ".--":
			return 'W';
		case "-..-":
			return 'X';
		case "-.--":
			return 'Y';
		case "--..":
			return 'Z';
		default:
			throw new IllegalArgumentException(String.format(
					"Neplatný morse kód: '%1$s'", code));
		}
	}

}

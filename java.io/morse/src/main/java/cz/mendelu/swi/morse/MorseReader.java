package cz.mendelu.swi.morse;

import java.io.IOException;
import java.io.Reader;

public class MorseReader extends Reader {

	private final Reader in;

	private final MorseCode morseCode;

	/**
	 * Veřejný konstruktor.
	 * @param in vsupem je textový soubor obsahující text v moresově abecedě odělený pomocí svislích lomítek.
	 */
	public MorseReader(Reader in) {
	this.in = in;
	this.morseCode = new MorseCode();
	}

	@Override
	public void close() throws IOException {
		throw new UnsupportedOperationException("TODO");
	}
	
	/**
	 * metoda na čtení
	 */
	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
	    throw new UnsupportedOperationException("TODO");
	}

}
